# Diagrama de clases
---


```mermaid
classDiagram

    class Estado {
        <<enumeration>>
        ACTIVO
        INACTIVO
    }

    class Rol {
        id UUID
        nombre Rolname
        descripcion String
        estado Estado
    }

    class Perfil {
        id UUID
        apellidos String
        nombres String
        cargo String
    }

    class Usuario {
        id UUID
        username String
        email String
        estado Estado
        rol Rol
    }

    class Esquema {
        id UUID
        nombre String
        esquemaId String
    }

    class Anio {
        id UUID
        nombre String
        enumeracion String
    }

    class AnioPlanEstudio {
        id UUID
        node_id String
    }

    class PlanDeEstudio {
        id UUID
        nombre String
        codigo String
        descripcion String
        workspace String
        esquemas List~Esquema~
        anioPlanEstudio List~AnioPlanEstudio~
    }

    class Materia {
        id UUID
        nombre String
        codigo String
        regimen String
        horasSemanales Number
        horasTotales Number
        nodoId String
    }

    class NodoMateria {
        id UUID
        nombre String
        nodoId String
    }

    class Auditoria {
        id UUID
        evento String
        estado Estado
        payload String
        fechaCreacion Datetime
    }

    class Propuesta {
        id UUID
        nodoId String
        estado Estado
        contenidoMinimo String
        fundamentos String
        objetivos String
        metodologia String
        evaluacion String
        bibliografiaGeneral String

    }

    class Correlativa {
        id UUID
        nombre String
        idRelacionNeural String
        necesariaParaCursar Number
        necesariaParaRendirFinal Number
        estado Estado
    }

    class Bibliografia {
        id UUID
        nombre String
        isbn String
        autor String
        estado Estado
        nodoId String

    }

    class Unidad {
        id UUID
        numero Number
        nombre String
        objetivos String
        estado Estado
        nodoId String
    }

    class Contenido {
        id UUID
        nombre String
        descripcion String
        estado Estado
        esMinimo Boolean
        unidad Unidad
    }


    %% Relaciones
    Rol "1" -- "1" Estado
    Rol "1" -- "1" Usuario
    Usuario "1" -- "1" Perfil
    PlanDeEstudio o-- Esquema
    AnioPlanEstudio --* PlanDeEstudio
    AnioPlanEstudio --* Anio
    AnioPlanEstudio *-- Materia
    NodoMateria --* Materia
    Auditoria *-- Usuario
    Propuesta "1" -- "1" Materia
    Materia "1..*" -- "1" Correlativa : "es correlativa"
    Materia "1..*" -- "1" Correlativa : "es materia"
    Materia *-- Bibliografia
    Materia o-- Usuario : "tiene docentes"
    Materia *-- Unidad
    Unidad *-- Contenido

    
```