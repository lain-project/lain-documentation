```mermaid
gantt
        dateFormat  YYYY-MM-DD
        title Asistente Virtual UNdeC
        section Anteproyecto
        Capacitación en redacción de objetivos  : 2020-04-01, 2020-04-02
        Denominación del tema elegido           : 2020-04-02, 2020-04-03
        Definir las areas de conocimiento       : 2020-04-03, 2h
        Describir el tema elegido               : 2020-04-05
        Establecer los objetivos generales      : 2020-04-05, 2020-04-07
        Definir los objetivos especificos       : 2020-04-07, 2020-04-09
        Definir el alcance del trabajo          : 2020-04-07, 2020-04-09
        Definir los limites del trabajo         : 2020-04-09, 2020-04-11
        Describir la metodologia a emplear      : 2020-04-10, 2020-04-12
        Analisis de factibilidad                : 2020-04-12, 2020-04-13
        Documentación del anteproyecto          : 2020-04-04, 2020-04-14
        Capacitación en Zotero                  : 2020-04-15, 2020-04-17
        Definir alcances y objetivos del nuevo sistema  : 2020-06-17, 2020-06-20
        Definicion de requerimientos de usuario : 2020-06-18, 2020-06-24

        section Estimación de costo y calendario
        Definición y descripción de las actividades :2020-04-26, 2020-06-04
        Definir los roles de cada implicado         :2020-06-17, 2020-06-19
        Definir perfil empleado para cada implicado :2020-06-08, 2020-06-10
        Estimación de tiempos para tarea            :2020-06-11, 2020-06-16
        Estimación de recursos a utilizar en cada tarea :2020-06-13, 2020-06-15
        Diagrama de tiempos                         :2020-06-05, 2020-06-16
        Estimación de costos y calendario           :2020-06-14, 2020-06-16
        Documentar planificación                    :2020-04-28, 2020-06-15

        section Identificacion inicial de requerimientos
        Investigación de antecedentes               :2020-04-18, 2020-04-21
        Analizar y seleccionar software antecesores :2020-04-23, 2020-04-25
        Relevamiento general de los sistemas        :2020-05-04, 2020-05-19
        Analisis de funciones detectadas en los sistemas    :2020-05-16, 2020-05-20
        Analisis de tecnologias detectadas                  :2020-05-20, 2020-06-05
        Explicación de cada funcionalidad           :2020-06-06, 2020-06-16
        Especificar los problemas y necesidades     :2020-04-26, 2020-06-16
        Documetación del relevamiento               :2020-04-21, 2020-06-16

        section Preparación del entorno del proyecto
        Investigación de herramientas               :2020-04-15, 2020-04-17
        Definicion de las herramientas              :2020-04-22, 2020-04-23
        Capacitación en herramientas                :2020-04-23, 2020-04-26
        Establecer los metodos de comunicación      :2020-05-07, 2020-05-10
        Organizar la ejecución del proyecto         :2020-06-05, 2020-06-07
        Establecer las herramientas                 :2020-06-12, 2020-06-16        
        %% Modulo de planes de estudio
        %% Modulo de chatbot
        %% Modulo de reportes

        %% section Implementación

        %% section Pruebas

        %% section Administración del proyecto

        %% section Entorno


        section Inicio
        Modelado de requerimientos de alto nivel    :2020-07-01, 2020-07-10
        Modelado de la arquitectura de alto nivel   :2020-07-12, 2020-07-16
        Prototipado de interfaces de usuario        :2020-07-18, 2020-08-19
        Realización del plan de pruebas inicial     :2020-07-19, 2020-07-30
        Realización inicial de producto del proyecto   :2020-08-01, 2020-08-14
        Realización inicial de modelos                 :2020-08-15, 2020-09-10
        Establecer el entorno de trabajo               :2020-09-12, 2020-10-30
        Objetivos del Ciclo de Vida                    :milestone, 2020-10:30

        section Elaboración
        Modelado de la Arquitectura                 :2020-11-02, 2020-12-10
        Prototipado de interfaces de usuario        :2021-03-12, 2021-06-05
        Generación de historias de usuario          :2021-03-12, 2021-06-16
        Pruebas de la arquitectura                  :2021-06-12, 2021-12-16
        Evolución de su modelo de pruebas           :2021-06-12, 2021-12-16
        Control de Administración de la Configuración :2021-11-03, 2022-02-05
        Capacidad Operativa Inicial                 :milestone, 2022-02-05

        section Construcción
        Modulo de seguridad                         :active, 2022-02-03, 2022-04-03
            Elaboración de diagramas                    :2022-02-03, 2022-02-10
            Desarrollo de las historias de usuario      :2022-02-10, 2022-02-20
            Actualizar plan de pruebas                  :2022-02-20, 2022-02-23
            Codificación del modulo                     :2022-02-23, 2022-03-23
            Pruebas                                     :2022-03-23, 2022-03-30
            Scripts de generación de datos              :2022-03-30, 2022-04-02
            Correccion de errores                       :2022-03-23, 2022-03-30
            Documentación                               :2022-03-30, 2022-04-03

        Modulo de plan de estudios                  :active, 2022-04-03, 2022-09-29
            Elaboración de diagramas                    :2022-04-03, 2022-04-15
            Desarrollo de las historias de usuario      :2022-04-15, 2022-05-20
            Actualizar plan de pruebas                  :2022-05-20, 2022-05-30
            Codificación del modulo                     :2022-05-30, 2022-08-21
            Pruebas                                     :2022-08-23, 2022-09-20
            Scripts de generación de datos              :2022-09-20, 2022-09-24
            Correccion de errores                       :2022-08-23, 2022-09-20
            Documentación                               :2022-09-24, 2022-09-29
        
        Modulo de chatbot                           :active, 2022-09-29, 2022-11-08
            Elaboración de diagramas                    :2022-09-29, 2022-10-07
            Desarrollo de las historias de usuario      :2022-10-07, 2022-10-23
            Actualizar plan de pruebas                  :2022-10-23, 2022-10-25
            Codificación del modulo                     :2022-10-25, 2022-11-04
            Pruebas                                     :2022-11-04, 2022-11-06
            Correccion de errores                       :2022-11-04, 2022-11-06
            Documentación                               :2022-10-07, 2022-11-08

        Modulo de Reporte                           :active, 2022-11-09, 2022-11-30
            Elaboración de diagramas                    :2022-11-09, 2022-11-15
            Desarrollo de las historias de usuario      :2022-11-09, 2022-11-15
            Actualizar plan de pruebas                  :2022-11-15, 2022-11-18
            Codificación del modulo                     :2022-11-15, 2022-11-23
            Pruebas                                     :2022-11-23, 2022-11-25
            Correccion de errores                       :2022-11-23, 2022-11-25
            Documentación                               :2022-11-25, 2022-11-30
        Capacidad Operativa Inicial                 :milestone, 2022-11-30
        
        section Transición
        Corrección de errores                       :2022-11-30, 2022-12-16
        Validación del sistema                      :2022-12-05, 2022-12-05
        Actualización de Documentación              :2022-12-05, 2022-12-12
        Manual de usuarió                           :2022-12-05, 2022-12-12
        Presentación del sistema                    :2022-12-16, 2022-12-16
        Liberación del Producto                     :milestone, 2022-12-16





















        %% Capacitación en redacción de objetivos  :a1, 2020/04/01, 1d 
        %% Completed task            :done,    des1, 2014-01-06,2014-01-08
        %% Active task               :active,  des2, 2014-01-09, 3d
        %% Future task               :         des3, after des2, 5d
        %% Future task2               :         des4, after des3, 5d
        %% section Critical tasks
        %% Completed task in the critical line :crit, done, 2014-01-06,24h
        %% Implement parser and jison          :crit, done, after des1, 2d
        %% Create tonests for parser             :crit, active, 3d
        %% Future task in critical line        :crit, 5d
        %% Create tests for renderer           :2d
        %% Add to mermaid                      :1d
```
