# SEG-03 Salir del sistema
---


```mermaid
flowchart TD

AA[Inicio] --> A
A(Cerrar Session) --> B(Elimina token de autenticacion)
B --> C(Refrescar el navegador)
C --> FIN
```