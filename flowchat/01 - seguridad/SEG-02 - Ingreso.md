# SEG-02 Ingreso al sistema
---


```mermaid
flowchart TD

AA[Inicio] --> A
A(Ingresar cuenta de google) --> B(Verifica token google)
B --> C[(Busca usuario por email)]
C --> D{"¿Existe el usuario?"}
D --> |No| E(El usuario con el mail no existe)
E --> FIN
D --> |Si| F(Genera token de seguridad)
F --> G(Obtiene token Neuralactions)
G --> H(Muestra pantalla de Inicio)
H --> FIN
  






```