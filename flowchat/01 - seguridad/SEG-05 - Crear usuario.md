# SEG-05 Crear usuario
---


```mermaid
flowchart TD

AA[Inicio] --> A
A(Crear usuario) --> B{"¿Estan todos los datos cargados?"}
B -->|Si| C[("Buscar usuarios por username")]
C --> D{"¿Existe otro usuario con el mismo username o email?"}

<!-- C -->|No| D("No existen usuarios registrados")
C -->|Si| F("Mostrar listado de usuarios")
D --> Fin
F --> Fin -->
```