# SEG-01 Ingreso al sistema
---


```mermaid
flowchart TD
    AA[Inicio] --> A
    A(Ingresar username) --> AB{"¿Username vacio?"}
    AB --> |Si| AC("Username vacio")
    AC --> A
    AB --> |No| BC(Ingresar password)
    BC --> BD{"¿Password vacia?"}
    BD --> |No| B[(Buscar usuario por username)]
    BD --> |Si| BE("Mostrar 'Password vacia'")
    BE --> BD
    B --> C{"¿Existe el usuario?"}
    C --> |No| D("Mostrar 'Username y/o password incorrecto'")
    D --> FIN
    C --> |Si| F(verificar password)
    F --> G{"¿El password es valido?"}
    G --> |No| D
    G --> |Si| I(Generar Token)
    I --> J(Mostrar pantalla de inicio)
    J --> FIN  


```