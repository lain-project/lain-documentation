# SEG-04 Listar usuarios
---


```mermaid
flowchart TD

AA[Inicio] --> A
A(Listar usuarios) --> B[(Obtener usuarios)]
B --> C{"¿Existen otros usuarios?"}
C -->|No| D("No existen usuarios registrados")
C -->|Si| F("Mostrar listado de usuarios")
D --> Fin
F --> Fin
```