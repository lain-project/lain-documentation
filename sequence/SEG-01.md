# SEG-01 - Ingreso al sistema con google
```mermaid
sequenceDiagram
    autonumber
    actor Usuario
    participant AuthControllerAdapter
    participant IngresoConGoogleUseCase
    participant GoogleRepository
    participant UsuarioRepository
    participant SeguridadDomainService
    participant NeuralActionsDomainService

    Usuario ->>+ AuthControllerAdapter: "Sign in"(token_id)
    AuthControllerAdapter ->>+IngresoConGoogleUseCase: execute(token_id)
    IngresoConGoogleUseCase ->>+ GoogleRepository: verificacion_google(tokenId): string
    alt token valido
        GoogleRepository-->>-IngresoConGoogleUseCase: return(email) 
    else token invalido
        GoogleRepository-->>IngresoConGoogleUseCase: raise(El usuario con email no existe)
    end
    IngresoConGoogleUseCase ->>+UsuarioRepository: buscarPorMail(email)
     alt el usuario existe
        UsuarioRepository-->>-IngresoConGoogleUseCase: return(usuario) 
    else el usuario no existe
        UsuarioRepository-->>IngresoConGoogleUseCase: return(None)
    end
    
    IngresoConGoogleUseCase ->>+ SeguridadDomainService: obtenerToken()
    SeguridadDomainService -->>- IngresoConGoogleUseCase: return(token)

    IngresoConGoogleUseCase ->>+ NeuralActionsDomainService: obtenerToken()
    NeuralActionsDomainService -->>- IngresoConGoogleUseCase: return(token)
    

    IngresoConGoogleUseCase -->>- AuthControllerAdapter: return(token_rest)
    AuthControllerAdapter -->>-Usuario:Pantalla de inicio 
    
    

```