# REP-01 


```mermaid
sequenceDiagram
    autonumber
    actor ADMIN as Administrador
    participant UI as Interfaz Reporte
    participant CTRL as ReporteController
    participant UC as ReporteService
    participant PREG_REPO as PreguntaRepository
    
    ADMIN ->>+ UI: Generar Reporte
    UI ->>+  CTRL: obtenerReporteConteoIntenciones(inicio, fin)
    CTRL ->>+ UC: contarIntenciones(inicio, fin)
    UC ->>+ PREG_REPO: buscarPorFechas(inicio, fin)
    PREG_REPO -->>- UC: return(preguntas)
    UC ->> UC: agruparIntencionesYContarlas(preguntas)
    UC -->>- CTRL: return(intencionesContadas)
    CTRL-->>-UI: return(intencioneContadas)
    UI -->>- ADMIN: Mostrar Reporte
   

```