# CHAT-01 - Sincronizar 

```mermaid
sequenceDiagram
    autonumber
    actor ADMIN as Administrador
    participant UI as Interfaz chatbot
    participant CTRL as ChatbotController
    participant UC as SincronizarDatosUseCase
    participant MAT_REPO as MateriaRepository
    participant PLA_REPO as PlanDeEstudioRepository
    participant CTRL_ENT as EntidadesController
    participant Dialogflow
    
    ADMIN ->>+ UI: sincronizar()
    UI ->>+  CTRL: sincronizar()
    CTRL ->>+ UC: execute()
    critical 
        UC ->> UC: sincronizarBasesDeDatos()
    end
    par 
        UC ->>+ MAT_REPO: obtenerMaterias()
        MAT_REPO -->>- UC: return(materias) 
        UC ->>+  PLA_REPO: obtenerPlanes()
        PLA_REPO -->>- UC: return(planes)
    end
    UC ->> UC: generarEntidadesYSinonimos()
    UC ->>+ CTRL_ENT: crearEntidades(entidadesYSinonimos)
    loop
        CTRL_ENT ->>+ Dialogflow: crearEntidades(entidades)
        Dialogflow -->>- CTRL_ENT: return(resultado)
    end
    loop 
        CTRL_ENT ->>+ Dialogflow: crearSinonimos(id, sinonimo)
        Dialogflow -->>- CTRL_ENT: return(resultado)
    end

    CTRL_ENT -->>- UC: return(resultado)
    UC -->>- CTRL: return(resultado)
    CTRL -->- UI: return(boolean)
    UI -->>- ADMIN: mostrarMensaje()


```