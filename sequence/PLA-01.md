# PLA-01 - Crear plan de estudio

```mermaid
sequenceDiagram
    autonumber
    actor Administrador
    participant UI as Interfaz Plan de estudio
    participant PlanDeEstudioController
    participant CrearPlanDeEstudioUseCase
    participant PlanDeEstudioRepository
    participant NeuralActionsDomainService
    participant Neuralactions
    participant EsquemaRepository
    Administrador ->>+ UI: guardar(plan)
    alt datos completos
        UI ->>+ PlanDeEstudioController: crearPlanDeEstudio(plan)
    else datos incompletos
        UI -->> Administrador: mostrarError()
    end

    PlanDeEstudioController ->>+ CrearPlanDeEstudioUseCase: execute(plan, años)
    CrearPlanDeEstudioUseCase ->>+ PlanDeEstudioRepository: buscarPorCodigo(codigo)
    alt el codigo no existe
        PlanDeEstudioRepository ->>- CrearPlanDeEstudioUseCase: return(None)
    else el codigo existe
        PlanDeEstudioRepository ->> CrearPlanDeEstudioUseCase: return(plan)
    end
    CrearPlanDeEstudioUseCase ->> CrearPlanDeEstudioUseCase: validarUnicidadDelCodigo(codigo)
    CrearPlanDeEstudioUseCase ->>+ NeuralActionsDomainService: crearWorkspace(plan)
    NeuralActionsDomainService ->>+ Neuralactions : crearWorkspace(plan)
    Neuralactions -->>- NeuralActionsDomainService: return(workspaceId)
    NeuralActionsDomainService -->>- CrearPlanDeEstudioUseCase: return(workspaceId)
    CrearPlanDeEstudioUseCase ->>+ NeuralActionsDomainService: crearEsquemas(workspaceId)
    par crear esquemas
        NeuralActionsDomainService ->>Neuralactions: crearEsquemaAnio(workspaceId, esquema)
        NeuralActionsDomainService ->>Neuralactions: crearEsquemaBibliografia(workspaceId, esquema)
        NeuralActionsDomainService ->>Neuralactions: crearEsquemaContenidoMinimo(workspaceId, esquema)
        NeuralActionsDomainService ->>Neuralactions: crearEsquemaDescriptor(workspaceId, esquema)
        NeuralActionsDomainService ->>Neuralactions: crearEsquemaDocente(workspaceId, esquema)
        NeuralActionsDomainService ->>Neuralactions: crearEsquemaMateria(workspaceId, esquema)
        NeuralActionsDomainService ->>Neuralactions: crearEsquemaPropuesta(workspaceId, esquema)
        NeuralActionsDomainService ->>Neuralactions: crearEsquemaNodo(workspaceId, esquema)
        NeuralActionsDomainService ->>Neuralactions: crearEsquemaUnidad(workspaceId, esquema)
    end
    Neuralactions -->>NeuralActionsDomainService: return(esquemas)
    NeuralActionsDomainService -->>- CrearPlanDeEstudioUseCase: return(esquemas)

    CrearPlanDeEstudioUseCase ->>+ EsquemaRepository: guardar(esquemas, idPlan)
    EsquemaRepository -->>- CrearPlanDeEstudioUseCase: return(None)
    CrearPlanDeEstudioUseCase -->>+ PlanDeEstudioRepository: guardar(plan)
    PlanDeEstudioRepository -->>- CrearPlanDeEstudioUseCase: return(plan)
    CrearPlanDeEstudioUseCase -->>- PlanDeEstudioController: return(plan)
    PlanDeEstudioController -->>- UI: return(boolean)
    UI -->>- Administrador: ConfirmacionDeCreacionExitosa


```